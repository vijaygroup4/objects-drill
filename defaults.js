//defaults function
function defaults(object, defaultObject) {
  for (let key in defaultObject) {
    //checking if object has that property
    if (object.hasOwnProperty(key) === false) {
      object[key] = defaultObject[key];
    }
  }
  return object;
}

//exporting the above function
module.exports = defaults;
