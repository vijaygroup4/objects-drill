//pairs function
function pairs(object) {
  let newArray = [];
  for (let key in object) {
    newArray.push([key, object[key]]);
  }

  return newArray;
}

//exporting the above function
module.exports = pairs;
