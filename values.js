//values function
function values(object) {
  let valuesArray = [];
  for (let key in object) {
    valuesArray.push(object[key]);
  }

  return valuesArray;
}

//exporting the above function
module.exports = values;
