//mapObject function
function mapObject(object, callback) {
  let newObject = {};
  for (let key in object) {
    newObject[key] = callback(object[key], key);
  }

  //returning the new object
  return newObject;
}

//exporting the above function
module.exports = mapObject;
