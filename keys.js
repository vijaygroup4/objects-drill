//keys function
function keys(object) {
  let newArray = [];
  for (let key in object) {
    newArray.push(String(key));
  }

  return newArray;
}

//exporting the above function
module.exports = keys;
