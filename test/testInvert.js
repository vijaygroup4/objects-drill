//importing the testObject
let testObject = require("./testObject");

//importing the invert function
let invert = require("../invert");

let invertedObject = invert(testObject);
console.log("testObject:", testObject);
console.log("invertedObject:", invertedObject);
