//creating testObject
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//exporting the above object
module.exports = testObject;
