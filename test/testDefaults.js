//importing testObject
let testObject = require("./testObject");

//creating defaultObject
let defaultObject = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
  gender: "male",
};

//importing defaults function
let defaults = require("../defaults");

let newObject = defaults(testObject, defaultObject);
console.log(newObject);
