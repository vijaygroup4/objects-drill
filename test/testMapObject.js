//importing the testObject
let testObject = require("./testObject");

//importing the mapObject function
let mapObject = require("../mapObject");

let doubleObject = mapObject(testObject, (value, key) => {
  //if it is a number I am doubling that value otherwise concating the values(string type)
  if (isNaN(value) === false) {
    return value + value;
  } else {
    return value + " " + value;
  }
});

console.log(doubleObject);
