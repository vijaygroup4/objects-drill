//invert function
function invert(object) {
  let invertedObject = {};
  for (let key in object) {
    invertedObject[object[key]] = key;
  }

  return invertedObject;
}

//exporting the above function
module.exports = invert;
